import HomeObjects from "../objects/homePageObj.js"
import SearchObjects from "../objects/searchPageObj.js"
var home = new HomeObjects()
var search = new SearchObjects()

describe('When successful search', () => {

    it('should contain at least one result', () => {  
        cy.visit('/')
        cy.get(home.searchButton).click()
        cy.get(home.searchField).type('NFL')
        cy.get(home.submitButton).click()
        cy.get(search.resultList).its('length').should('be.gte', 1)
    })

})

describe('When unsuccessful search', () => {

    it('should show no result list ', () => {  
        cy.visit('/')
        cy.get(home.searchButton).click()
        cy.get(home.searchField).type('NFLFake')
        cy.get(home.submitButton).click()
        cy.get(search.noResultList).should('exist')
    })

})
