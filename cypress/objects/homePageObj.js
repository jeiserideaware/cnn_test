class Home_objects {
    constructor() {
        this.searchButton = '[id="search-button"]';
        this.searchField = '[id="search-input-field"]';
        this.submitButton = '[id="submit-button"]';
    }
}
export default Home_objects